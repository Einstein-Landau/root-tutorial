# Summary

* [Introduction](README.md)
* [Interacting with ROOT](lesson-1.md)
* [TSelector](tselector.md)
* [RDataFrame](rdataframe.md)
* [Exercise 1: Exploring an NTuple](tutorial-1.md)
* [Fitting functions](fitting-functions.md)
* [Exercise 2: Fitting data](tutorial-2.md)
* [Exercise 3: Background subtraction](tutorial-3.md)
* [Exercise 4: Visualizing mathematics](tutorial-4.md)
* [Exercise 5: New data](tutorial-5.md)

