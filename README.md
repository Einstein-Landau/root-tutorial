# ROOT Tutorials

Written by Mike Sokoloff. Updated by Henry Schreiner.

This book is a conversion of the classic tutorial series ([available here](http://homepages.uc.edu/~sokoloff/P8015/)).

This is a gitbook. You can comment on any line using the plus on the far right of that line (assuming you are reading the web version, not the PDF or ebook versions). Feel free to suggest edits. You can ask to become an editor, as well.

## Using UC resources

ROOT 6 is installed on UC computers. The module system[^1] allows quick, easy access to ROOT and other packages[^2]. To use it, type:

{% terminal %}
uc:~ $ module load root
{% endterminal %}

into your prompt once signed in to a UC computer. You can see the all available modules for your current compiler, such as ROOT 5 and CUDA, with:

{% terminal %}
uc:~ $ module avail
{% endterminal %}


And you can save your loaded modules for later logins with

{% terminal %}
uc:~ $ module save
{% endterminal %}


### Data storage

The data for the exercises is in `/data/tutorial` on the UC systems. At some point, we are hoping to get permission to release some of this data for general access. The code is in the same location, or it can be obtained from [GitHub](https://github.com/henryiii/uc_root_tutorial.git). You can even use:

```bash
git clone https://github.com/henryiii/uc_root_tutorial.git
```

to download the script files to your current directory.


## Not using UC resources

If you are using your own computer, you'll need to find the `thisroot.sh` script that is inside the `bin` directory wherever you have ROOT installed. Then you need to source that file in your current terminal session:


{% terminal %}
local:~ $ source /path/to/bin/thisroot.sh
{% endterminal %}




[^1]: [LMod](https://www.tacc.utexas.edu/research-development/tacc-projects/lmod), if you are curious.
[^2]: If want to save keystrokes, you can use `ml` instead of `module` or `module load`.