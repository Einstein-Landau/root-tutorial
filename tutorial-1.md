# Exercise 1: Exploring an NTuple

{% hint style='info' %}
The following exercises relate to the NTUPLEs and ROOT code found in `/data/tutorial/`. To get access to ROOT on the Cincinnati systems, use `module load root` (`module save` will persist your modules). You'll want to have graphical access to the systems, usually by adding `-X -Y` to your ssh login command.{% endhint %}

## Interactive commands

There is file named `DVntuple-March04A.root`. You can create a
"logical link" to it in your subdirectory by executing the UNIX
command:

{% terminal %}
uc:~ $ ln -s /data/tutorials/DVntuple-March04A.root .
{% endterminal %}

Start ROOT by running the command `root`. Create a `TBrowser`
and find the names of the NTUPLEs
stored in this file.

One of the NTUPLEs obviously relates to the decay
$$ \Xi^- \to \Lambda \pi^- $$. Still in `TBrowser`, look at the
invariant mass distribution of the $$ \Xi^- \to \Lambda \pi^- $$
candidates.

Variables associated with the $$ \pi^- $$ candidate in this decay
have the prefix `PromptPi`. Variables associated with the
$$ \Lambda $$ candidate in this decay have the prefix `Lambda`.
Look at the invariant mass distribution of the
$$  \Lambda \to p \pi^- $$ candidates.

Using the interactive ROOT session, look first at the invariant
mass distribution of the $$ \Xi^- \to \Lambda \pi^- $$ candidates
and then at the invariant mass distribution of the
$$  \Lambda \to p \pi^- $$ candidates. To make the first
histogram, you can use

```cpp
XiDecayTuple->Draw("Xi_MM");
```

**What does `XiDecayTuple` mean here? What is the similar
command to look at the $$ \Lambda \to p \pi^- $$ candidates?**

You can make a histogram of the $$ \Xi^- $$ candidates with mass
in the range 1305 – 1340 MeV using the command

```cpp
XiDecayTuple->Draw("Xi_MM","1305<Xi_MM && Xi_MM<1340");
```

 Using a similar command, look at the $$ \Lambda \to p \pi^- $$
invariant mass distribution for "good"
$$ \Xi^- \to \Lambda \pi^- $$ candidates. **What does it mean to be
a "good" $$ \Xi^- \to \Lambda \pi^- $$ candidate?**

Using a similar command, look at the invariant mass distribution for "bad"
$$ \Xi^- \to \Lambda \pi^- $$ candidates. **What does it mean to be
a "bad" $$ \Xi^- \to \Lambda \pi^- $$ candidate?**

 We often identify $$ \Lambda \to p \pi^- $$ candidates as
`LambdaLL`s and `LambdaDD` according to whether the $$ p $$ or
$$ \pi $$ tracks (whose variables are denoted by the suffixes
`DecayPr` and `DecayPi`) are long tracks or downstream
tracks. These two categories are identified using the variables
`DecayPr_TRACK_Type` and `DecayPi_TRACK_Type`. **What
values denote each of the categories? How can you tell?**

Repeat the exercises of looking at invariant mass distributions for "good" and "bad" $$ \Xi^- $$
candidates separately for `LambdaLL` or `LambdaDD`
categories. **What conclusions can you draw?**


## Running an analysis script

{% hint style="info" %}
There are several ways to work with data in ROOT. The following describes the classic `MakeClass` system for historical reasons. The other common methods are listed here.

* **Draw**: The classic all-in-one method. Basically has its own mini-language. Besides making a plot, can also produce histograms and scatter plots. Described on [this page](https://root.cern.ch/doc/master/classTTree.html).
* **MakeClass**: Creates a header and source file for a specific TTree (NTuple). You fill in a method, do the looping yourself. You usually only edit the source file. ROOT 5+. See the [Nevis tutorial] before 2015.
* **MakeSelector**: Creates a more organized header and source file. Does the looping for you. You might edit the header to add histograms and other variables. Much faster, and can be used in multithreaded mode to some extent. Root 6+. See the most recent [Nevis tutorial].
* **RDataFrame**: ROOT 6.14+, provides an implicitly multithreaded system that queues commands until needed. Very new, might have some initial bugs. Use C++ or Python.

There is also a Python binding to ROOT, PyROOT, that is available. Also covered in the [Nevis tutorial].

[Nevis tutorial]: https://www.nevis.columbia.edu/~seligman/root-class/
{% endhint %}

Typing commands one at a time is great for quickly looking at data
stored in an NTUPLE. However, it becomes very awkward when you want
to do anything sophisticated. ROOT allows you to use *macros*,
files containing many commands which are executed from an
interactive session as a single unit. Not only does ROOT provide the
ability to use such files, it has commands to create macros
which know about your NTUPLE. This is also discussed starting in the 2014 (`MakeClass`) 
or 2017 (`MakeSelector`) [Nevis tutorial].

[Nevis tutorial]: https://www.nevis.columbia.edu/~seligman/root-class/

If you are running ROOT, quit and re-start it. Then, execute the
following commands:

{% codetabs name="MakeClass", type="cpp" %}auto f = new TFile("DVntuple-March04A.root")
myXiTuple->cd()
XiDecayTuple->MakeClass("Xi_Analysis")
{% language name="MakeSelector", type="cpp" %}auto f = new TFile("DVntuple-March04A.root")
myXiTuple->cd()
XiDecayTuple->MakeSelector("Xi_Selector")
{% language name="RDataFrame", type="cpp" %}ROOT::RDataFrame frame("DVntuple-March04A.root", "myXiTuple/XiDecayTuple")
{% endcodetabs %}

              
This produce a response similar to[^1]:

```
Info in <TTreePlayer::MakeClass>: Files: Xi_Analysis.h and 
Xi_Analysis.C generated from TTree: XiDecayTuple
```              

Later pages introduce methods to use a ROOT macro to make
histograms and scatter plots. Using these methods, edit your
`Xi_Analysis.C` or `Xi_Selector.C` file to make the same histograms you made typing
one-line commands in part (1).

> Note: If you use RDataFrame, you will not get any files. Instead, you'll implement everything on the RDataFrame.

[^1]: MakeSelector will complain about not knowing the type of the covariance matrices. That's okay, you won't use them.

