# Exercise 5: New data
{% hint style="info" %}
The following exercises relate to NTUPLEs found in
`/data/tutorials`.{% endhint %}

There are four new files:

-   `DVntuple-LambdaC-MagDown2011.root`

-   `DVntuple-LambdaC-MagUp2011.root`

-   `DVntuple-LambdaC-MagDown2012.root`

-   `DVntuple-LambdaC-MagUp2012.root`

These files are similar to `DVntuple-March04A.root`, but each contains
only one subdirectory, `myLcTuple`, rather than several. The
`DecayTree` tuple in this subdirectory may or may not have exactly the
same structure as that with the same name in `DVntuple-March04A.root`.
If it is, you should be able to edit the name of the input file in your
original analysis file (`MakeClass`) or run your selector on the new trees/chains (`MakeSelector`).
 If not, you will need to execute `Make...`
again and then copy your existing code over. (Be sure to either use a
different name for the call to `Make...` or copy your existing code
to files with different names so you don't overwrite the existing
files.)

Starting with the code you have already "optimized" for
$$ \Lambda_C \to \Xi^- K^+ \pi^+ $$,

-   Make the $$ \Lambda_C $$ (corrected) mass plot for each NTUPLE, first
    with the stripping level cuts and then with your optimized cuts. For
    each of these plots, fit the data to extract the mass of the
    $$ \Lambda_C $$, the width of the peak, and the number of signal
    events (and all the statistical errors). Are the masses and widths
    consistent with each other?

-   Use the `TChain` command to process the four new files as if they
    were one (see the [ROOT docs](https://root.cern.ch/how/how-use-chains-lists-files)).
    Again, make the mass plots with stripping level cuts and your
    optimized cuts, and fit them.

-   Define $$ \Lambda_C $$ signal and background ranges in
    $$ m( \Xi^- K^+ \pi^+ ) $$ and make a mass plot with the signal region
    shown in one color, the background in a second color, and the
    remaining bins in a third color.

-   Make the Dalitz plot for the signal region three times, once for
    each distinct pair of $$  (m_{ij}^2 , \, m_{jk}^2  )$$ on the axes.
    Also make the projections.

-   Make a background-subtracted Dalitz plot for whichever one set of
    $$ ( m_{ij}^2 , \, m_{jk}^2  ) $$ axes is most sensible to you. To do
    this, you should consider what binning will provide enough
    granularity to see signal structure while not leading to very large
    statistical fluctuations in background. (This is probably easier
    said than done.) Make all three $$ m_{ij}^2 $$ distributions as
    bakground-subtracted histograms.

-   How would you describe what you observe? Draw a corresponding
    Feynman diagram (this will require popping $$ q \, \bar q $$ pair out
    of the vacuum, or radiated by a gluon, in addition to a tree-level
    amplitude) that "explains" what you observe. What other Feynman
    diagrams can you draw which lead to $$ \Xi^- K^+ \pi^+ $$ final
    states? How does the Dalitz plot help discriminate between various
    amplitudes in this decay? Look at <https://pdglive.lbl.gov>
    to see what is known about the structure(s) you observe in the
    Dalitz plot, and read the most relevant paper(s).


