# Exercise 4: Visualizing mathematics


This exercise is a bit different from those we’ve done already. Rather
than using data in an NTUPLE, we will visualize mathematical equations.
Specifically, we will look at decay rates predicted by isobar models of
three-body decays as function of position in the Dalitz plot
(two-dimensional phase space). To be specific, we will consider the
decay $$ D^0 \to K^0_S \pi^- \pi^+ $$ The basic formalism for Dalitz plot
amplitudes is discussed in Section 2.1 of
[Rolf’s thesis](http://rave.ohiolink.edu/etdc/view?acc_num=ucin1285687286), and a
discussion of the isobar model in Sections 5.1 and 5.2. Reading these
first will help you do the work and understand the underlying physics.
There are three major steps:

-   Create a TH2D object and fill the area within the Dalitz plot
    boundary with a uniform (non-zero) value. This corresponds to a
    uniform matrix element.

-   Now, make a Dalitz plot for *one* of the amplitudes listed in
    Table 5.1 of Rolf’s thesis. Note that the formulas in Eqns. 2.13 -
    2.17 tell you how to construct the quantum mechanical
    *amplitude*. To get the rate (which is what is plotted in the
    Dalitz plot), you need to calculate $$ | {\cal M}|^2 $$. To make life
    a little easier, approximate $$ F_D $$ and $$ F_D $$ as unity. While not
    precisely true, it is good enough to understand the most important
    aspects of Dalitz plots.

-   An especially interesting feature of the Dalizt plot is that it
    allows you to visualize macroscopic quantum interference. Choose two
    of the amplitudes in Table 5.1 of Rolf’s thesis and make the
    corresponding Dalitz plot. Then, change the phase of `*one*`
    (multiplying by $$ e^{i \pi / 2}$$, for example), and make the Dalitz
    plot again.

When you have completed one example of each of these three steps, there
are several variations on a theme.

-   Make a Dalitz plot with all the Cabibbo-favored amplitudes in Table
    5.1 (when the resonance is neutral or a $$ K^{*-} $$ of some sort, or
    non-resonant). Then, add the doubly Cabibbo-suppressed (DCS)
    amplitudes (when the resonance is a $$ K^{*+} $$ of some sort). Where
    do you see the DCS amplitudes, and what are the effects?

-   With two amplitudes, systematically change the phase between the two
    in realtively small increments (for you to determine) and describe
    how the interference pattern changes. If you are feeling very
    energetic, learn how to make a gif animation on a web page, and use
    one to illustrate the variations. I don’t know how to do this, but
    I should be able to help you find help if you are so inclined.

There are a number of methods for visualizing a two-dimensional plot in
ROOT. These include scatter plots, contour plots, lego plots, color
plots, and box plots. For each exercise, make plots using at least three
of these methods and decide which one is best, or which ones are best
for seeing various features.
