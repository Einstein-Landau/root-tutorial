# Exercise 2: Fitting data

{% hint style="info" %}

You should already have a link to `/data/tutorial/DVntuple-March04A.root`. The scripts you look at should be copied into your directory instead of linked (`cp`). The scripts are also [available on GitHub](https://github.com/henryiii/uc_root_tutorial.git).{% endhint %}

There is a ROOT macro `FittingExample.C` (`MakeClass`) or `FittingSelector.C` (`MakeSelector`) in the subdirectory
(along with the corresponding header file). It
illustrates how to fit data using an external function. These files
were created using one of the methods described in
the first set of ROOT exercises.

The code uses a 1 MeV bin gaussian with linear signal described in , with the name `fit1MeV_Gaussian.C`.
**Write, and test, a similar `fit2MeV_Gaussian.C` fitting function.**
To illustrate how it works, make two histograms of the same quantity
using different binnings. Fit each with the appropriate
`fitMeV_Gaussian`. Make plots of each with the results reported
in a box in the plot.

Inside `DVntuple-March04A.root` there is a subdirectory called
`myLcTuple` with an NTUPLE called `DecayTree`. It has candidates
for the decay $$ \Lambda_c \to \Xi^- K^+ \pi^+$$.
Use `Make...(...)` to create an analysis macro for this NTUPLE.

Define a set of good $$ \Xi^- $$ candidates. Then make (and fit) histograms of $$ \Lambda_c $$ candidates varying the selection criteria for $$ \Xi^{-} $$ candidates, the transverse momentum of the $$ \Lambda_c $$ (`LambdaC_PT`) (units are MeV), and the decay time of the $$ \Lambda_c $$ (`LambdaC TAU`) (units are ps of proper decay time). **Do your results make sense? If not, why not? If yes, explain carefully.**


